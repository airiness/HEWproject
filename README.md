# HEWproject
***
A console game made in period of "HAL Event Week ".<br> 
Game Title is "Spladouble"<br> 
game rule is like "splatoon":the player who occupies the bigger area will win in 60 seconds.<br> 

## window size
Width:`120`<br>
Height:`40`<br>

## operate method:
Keyboard<br> 
Green Player:<br>
Left`A` Right`D` Up`W` Down`S`<br>
Shoot`F` Supply Ink`G`<br> 
Red Player:<br>
Left`←` Right`→` Up`↑` Down`↓`<br>
Shoot`Numeric keypad1` Supply Ink`Numeric keypad2`<br> 

## screenshots:
***
title:<br>
![](images/hewTitle.png)
***
in game:<br>
    *the color on the face is HP<br>
![](images/hewIngame.png)
***
result:<br>
    *bigger area win<br>
![](images/hewResult.png)

